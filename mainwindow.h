#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTreeWidget>


namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_actionRead_triggered();
    void ReadToFile(const QString &filePath);
    void PrintDir(const QString &filePath);
    QString PrintDir2(QString path, int level);
    QString PrintDir2_1(QString path, int level, QTreeWidgetItem *itm);

    void on_actionWrite_to_file_triggered();

    void on_actionPrint_dir_triggered();

    void on_pushButton_2_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
