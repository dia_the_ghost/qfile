#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QMessageBox>
#include <QFile>
#include <QDir>
#include <QTextStream>
#include <QDebug>
#include <QTreeWidgetItem>
#include "dialog.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    //setCentralWidget(ui->textEdit);
    qDebug()<<"Central widget is set!";

//    ui->comboBox->clear();
//    for(int i=1; i<=10; ++i)
//        ui->comboBox->addItem(QString::number(i));

//    for(int i=1; i<=10; ++i)
//        ui->listWidget->addItem(QString::number(i));

//    QTreeWidgetItem *itm = new QTreeWidgetItem(2);  //2 �������
//    QTreeWidgetItem *child1 = new QTreeWidgetItem(2);
//    QTreeWidgetItem *child2 = new QTreeWidgetItem(2);

//    itm->setText(0, "Hello");           //����� ������� 0
//    itm->setText(1, "description");

//    child1->setText(0, "eer");
//    child2->setText(0, "eertt");

//    itm->addChild(child1);
//    itm->addChild(child2);

//    ui->treeWidget->setColumnCount(2);
//    ui->treeWidget->addTopLevelItem(itm);

    ui->treeWidget->setColumnCount(2);



    ui->statusBar->showMessage("Ready to work!");
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionRead_triggered()
{
    Dialog *wnd = new Dialog(this);
    wnd->show();

    connect(wnd, SIGNAL(FilePath(QString)), this, SLOT(ReadToFile(QString)));
}

void MainWindow::ReadToFile(const QString &filePath)
{
    QFile mFile(filePath);

    if(!mFile.open(QFile::ReadOnly | QFile::Text))
    {
        QMessageBox::information(this, "Error!","Path is not correct");
        return;
    }

    QTextStream stream(&mFile);
                                        //QString  buffer = stream.operator >>();//������ �� ��������� ������
    QString  buffer = stream.readAll();

    ui->textEdit->setText(buffer);
    ui->statusBar->showMessage("Read to file!");
    mFile.flush();
    mFile.close();
}


void MainWindow::on_actionWrite_to_file_triggered()
{
    QFile mFile("text.txt");

    if(!mFile.open(QFile::WriteOnly | QFile::Text))
    {
        QMessageBox::information(this, "Error!","Path is not correct");
        return;
    }

    QTextStream stream(&mFile);
    stream << ui->textEdit->toPlainText();

    ui->statusBar->showMessage("Written to file");

    mFile.close();
}


void MainWindow::on_actionPrint_dir_triggered()
{
    Dialog *wnd = new Dialog(this);
    wnd->show();

    connect(wnd, SIGNAL(FilePath(QString)), this, SLOT(PrintDir(QString))) ;
}


// ������� ��� ������������ ��������� �����
//void MainWindow::PrintDir(const QString &filePath)
//{
//    ui->statusBar->showMessage("START");
//    QDir mDir(filePath);
//    QString buffer;

//    QList<QFileInfo> listFileInfo = mDir.entryInfoList();
//    for(int i = 0; i < listFileInfo.size(); i++)
//    {
//        buffer += listFileInfo[i].absoluteFilePath() + '\n';
//    }

//    mDir.mkdir("Hello");
//    ui->textEdit->setText(buffer);
//}

void MainWindow::PrintDir(const QString &filePath)
{
    ui->textEdit->setText(PrintDir2(filePath,0));
}



QString MainWindow::PrintDir2_1(QString path, int level, QTreeWidgetItem *itm)
{
    int check = 1;
    QString space;
    QString buffer;
    QDir folder(path);

    QList<QFileInfo> listFileInfo = folder.entryInfoList();

    for (int i=0; i<level; ++i)
        space += "      ";

    for(int i = 0; i < listFileInfo.size(); i++)
    {
        if(check>2)
        {
            QTreeWidgetItem *itm2 = new QTreeWidgetItem(2);
            itm2->setText(0, listFileInfo[i].fileName()); //1 ������� ��� �����
            itm2->setText(1, listFileInfo[i].absoluteFilePath());//2 ������� ����������
            itm->addChild(itm2);

            buffer += space + listFileInfo[i].absoluteFilePath() + '\n';

            if(listFileInfo[i].isDir())
                buffer += PrintDir2_1(listFileInfo[i].absoluteFilePath(), ++level, itm2);
        }
        check++;
    }
    ui->statusBar->showMessage("CORRECT");
    return buffer;
}

QString MainWindow::PrintDir2(QString path, int level)
{
    int check = 1;
    QString space;
    QString buffer;
    QDir folder(path);

    QList<QFileInfo> listFileInfo = folder.entryInfoList();

    for (int i=0; i<level; ++i)
        space += "      ";

    for(int i = 0; i < listFileInfo.size(); i++)
    {
        if(check>2)
        {
            buffer += space + listFileInfo[i].absoluteFilePath() + '\n';
            if(listFileInfo[i].isDir())
                buffer += PrintDir2(listFileInfo[i].absoluteFilePath(), ++level);
        }
        check++;
    }
    ui->statusBar->showMessage("CORRECT");
    return buffer;
}

void MainWindow::on_pushButton_2_clicked()
{
    //QMessageBox::information(this, "Title", ui->comboBox->currentText());

    //QListWidgetItem *itm = ui->listWidget->currentItem();
    //itm->setBackground(Qt::red); //QColor(0,55,255);
    //itm->setTextColor(Qt::blue);
    //QMessageBox::information(this, "Title", itm->text());

    //QMessageBox::warning(this, "warning", ui->treeWidget->currentItem()->text(0));

    QTreeWidgetItem *itm = new QTreeWidgetItem(2);
    itm->setText(0,"D:/qt/Projects/");

    PrintDir2_1("D:/qt/Projects", 0, itm);

    ui->treeWidget->addTopLevelItem(itm);


}
