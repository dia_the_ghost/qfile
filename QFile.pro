#-------------------------------------------------
#
# Project created by QtCreator 2015-05-25T02:00:07
#
#-------------------------------------------------

QT       += core gui

TARGET = QFile
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    dialog.cpp

HEADERS  += mainwindow.h \
    dialog.h

FORMS    += mainwindow.ui \
    dialog.ui

RESOURCES += \
    myRes.qrc
